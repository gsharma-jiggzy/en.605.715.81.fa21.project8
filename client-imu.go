package main

import (
    "fmt"
    "log"
    "net"
    "os"
    "go.bug.st/serial"
)

func main() {
    port, err := serial.Open("/dev/ttyACM1", &serial.Mode{})
    if err != nil {
        log.Fatal(err)
    }
    buff := make([]byte, 100)
    arguments := os.Args
    if len(arguments) == 1 {
        fmt.Println("Please provide host:port.")
        return
    }

    CONNECT := arguments[1]
    c, err := net.Dial("tcp", CONNECT)
    if err != nil {
        fmt.Println(err)
        return
    }
    for {
        // Reads up to 100 bytes
        n, err := port.Read(buff)
        if err != nil {
            log.Fatal(err)
        }
        if n == 0 {
            fmt.Println("\nEOF")
            break
        }

        fmt.Fprintf(c, string(buff[:n]))
    }
}